# Microservice architecture
Designing a microservice architecture with remote procedure call,
messaging and data caching. This project is under development. <br>

Main pods:
- [API Gateway](https://gitlab.com/archmicro/api-gateway)
- [Data microservice](https://gitlab.com/archmicro/data-service)
- [Highload microservice](https://gitlab.com/archmicro/highload-service)

Auxiliary pods:
- [Frontend](https://gitlab.com/archmicro/frontend)
- [Ingress](https://gitlab.com/archmicro/ingress)
- [Apache Kafka](https://gitlab.com/archmicro/kafka)
- [Redis](https://gitlab.com/archmicro/redis)
- [PostgreSQL](https://gitlab.com/archmicro/postgres)

![Diagram](https://gitlab.com/archmicro/readme/-/raw/main/ver/v0.2.jpg)
